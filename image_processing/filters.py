import numpy as np
import matplotlib.pyplot as plt
import cv2

# read in image file

image_path = './assets/water_balloons.jpg'

image = cv2.imread(image_path)
# print(type(image))

# copy image
image_copy = np.copy(image)
# print(type(image_copy))

# conver to RGB
image_copy = cv2.cvtColor(image_copy, cv2.COLOR_BGR2RGB)
# show unedited image
plt.figure()
plt.imshow(image_copy)


## convert to gray scale image_copy

gray = cv2.cvtColor(image_copy, cv2.COLOR_RGB2GRAY)
plt.figure()
plt.imshow(gray, cmap = 'gray')

## Vertical edge detector

sobel_kernel = np.array([ [-1, 0, 1],
                    [-2, 0, 2],
                    [-1, 0, 1] ])


# gaussian_blur_kernel = np.array([ [-1, 0, 1],
#                                 [-2, 0, 2],
#                                 [-1, 0, 1] ])



filtered_image = cv2.filter2D(gray, -1, sobel_kernel)

plt.figure()
plt.imshow(filtered_image, cmap = 'gray')


## Binary image

retval, binary_image = cv2.threshold(filtered_image, 100, 255, cv2.THRESH_BINARY)

plt.figure()
plt.imshow(binary_image, cmap = 'gray')

plt.show()
