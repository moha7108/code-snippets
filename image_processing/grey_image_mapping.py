import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import cv2 #opencv

image_path = 'path/to/image'
image = mpimg.imread(image_path)

print('Image dimensions: ', image.shape)


#change from color to grayscale

gray_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

plt.imshow(gray_image, cmap = 'gray')

# print specific grayscale pixel values

x = 190
y = 375

pixel_val = gray_image[y,x]
print(pixel_val)
