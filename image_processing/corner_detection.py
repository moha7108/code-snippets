import numpy as np
import matplotlib.pyplot as plt
import cv2
from load_image_module import load_image


image_copy, image_rgb, image_gray = load_image('./assets/waffle.jpg')

plt.figure()
plt.imshow(image_gray, cmap = 'gray')

image_gray = np.float32(image_gray)



dst = cv2.cornerHarris(image_gray,2,3, 0.04)

dst = cv2.dilate(dst,None)

plt.figure()
plt.imshow(dst, cmap = 'gray')

plt.show()
