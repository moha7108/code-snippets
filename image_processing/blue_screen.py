import matplotlib.pyplot as plt
import numpy as np
import cv2 #opencv


image_path = '../assets/pizza_bluescreen.jpg'

image = cv2.imread(image_path)
image_copy = np.copy(image)
image_copy = cv2.cvtColor(image_copy, cv2.COLOR_BGR2RGB)

print('this image is: ', type(image), ' with dimensions: ', image.shape)

plt.figure()
plt.imshow(image_copy)

# blue threshold
lower_blue = np.array([0,0,230])
upper_blue = np.array([50, 50, 255])

#image mask

mask = cv2.inRange(image_copy, lower_blue, upper_blue)
plt.figure()
plt.imshow(mask, cmap = 'gray')



masked_image = np.copy(image_copy)

masked_image[mask != 0] = [0,0,0]

# mask and add background image_path
background_image = cv2.imread('../assets/space_background.jpg')
background_image = cv2.cvtColor(background_image, cv2.COLOR_BGR2RGB)

crop_background = background_image[0:image.shape[0],0:image.shape[1]]
crop_background[mask == 0] = [0,0,0]

plt.figure()
plt.imshow(crop_background)

## combineing the output

complete_image = crop_background + masked_image

plt.figure()
plt.imshow(complete_image)
plt.show()
