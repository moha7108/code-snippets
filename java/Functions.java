import java.util.Scanner;

public class Functions{
    /*Built in Functions*/
    // System.out.println()
    // string.equals()
    //

    public static void announceDeveloperTeaTime(){

        System.out.println("Waiting for developer tea time...");
        System.out.println("Type in a random word and press Enter to start Developer tea time");
        Scanner input = new Scanner(System.in);
        input.next();
        System.out.println("its developer Tea time!");


    }

    public static double calculateTotal(double listedMealPrice, double tipRate, double taxRate){

        double tip = tipRate * listedMealPrice;
        double tax = taxRate * listedMealPrice;
        double result = listedMealPrice + tip + tax;
        System.out.println("your total meal price is " + result);

        return result;


    }

    public static void main(String[] args){

        // announceDeveloperTeaTime();

        double total = calculateTotal(15, .2, .08);
        System.out.println(total/2 + " /person");

    }

}
