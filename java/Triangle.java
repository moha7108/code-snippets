public class Triangle{

    /*attributes*/
    static int numOfSize = 3; //class or static variable that can be called just by calling class instead of instantiated object

    double base; // instance variable, created when instantiated with object
    double height;
    double sideLenOne;
    double sideLenTwo;
    double sideLenThree;

    /*construtor*/
    public Triangle(double base, double height, double sideLenOne, double sideLenTwo, double sideLenThree){ //This is the constructor
        this.base = base;
        this.height = height;
        this.sideLenOne = sideLenOne;
        this.sideLenTwo = sideLenTwo;
        this.sideLenThree = sideLenThree;
    }

    public double findArea(){
        return (this.base * this.height) / 2;
    }
    // note that there is static term added, making it a instance/nonstatic method rather than a class/static method (can only call with an instantiated object)


}
