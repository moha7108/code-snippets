import torch

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(f'{device} detecting, using {device} to train model')
