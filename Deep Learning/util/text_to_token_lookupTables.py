from collections import Counter

def create_lookup_tables(text):
    """
    Create lookup tables for vocabulary
    :param text: The paragraph texts of dataset split into words
    :return: A tuple of dicts (vocab_to_int, int_to_vocab)
    """
    counts = Counter(text)
    vocab = sorted(counts, key=counts.get, reverse = True)
    vocab_to_int = {}
    int_to_vocab = {}

    for ii, word in enumerate(vocab, 1):
        vocab_to_int[word] = ii
        int_to_vocab[ii] = word


    # return tuple
    return (vocab_to_int, int_to_vocab)
